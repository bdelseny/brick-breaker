/** @type {import('../../../typing/phaser')} */
const CENTER = canvas.width / 2;
const BOTTOM = canvas.height - 20;

const Breakout = new Phaser.Class({
    Extends: Phaser.Scene,

    initialize: function Breakout() {
        Phaser.Scene.call(this, { key: 'breakout' });
    },

    /**
     * Preload scene
     * @this Phaser.Scene
     */
    preload: function () {
        this.load.atlas(
            'assets',
            `${baseUrl}assets/breakout.png`,
            `${baseUrl}assets/breakout.json`
        );
    },

    /**
     * Create the Scene
     * @this Phaser.Scene
     */
    create: function () {
        this.lives = 3;
        this.score = 0;
        this.scoreText = this.add.text(10, 1, `Score: ${this.score}`, { fontSize: '24px', fill: 'white', fontFamily: 'sans' });
        this.livesText = this.add.text(950, 1, '❤'.repeat(this.lives), { fontSize: '24px', fill: 'red', fontFamily: 'sans', rtl: true });
        //  Enable world bounds, but disable the floor
        this.physics.world.setBoundsCollision(true, true, true, false);

        /**
         * Set bricks configuration
         * @param {number} frameQuantity The number of times each frame should be combined with one key.
         * @param {number} width The width of the grid in items (not pixels).
         * -1 means lay all items out horizontally, regardless of quantity.
         * If both this value and height are set to -1 then this value overrides it and the height value is ignored.
         * @param {number} height The height of the grid in items (not pixels).
         * -1 means lay all items out vertically, regardless of quantity.
         * If both this value and width are set to -1 then width overrides it and this value is ignored.
         * @param {number} cellWidth The width of the cell, in pixels, in which the item is positioned.
         * @param {number} cellHeight The height of the cell, in pixels, in which the item is positioned.
         * @returns {Phaser.Types.GameObjects.Group.GroupCreateConfig}
         */
        function makeGroupConfig(frameQuantity, width, height, cellWidth, cellHeight) {
            return {
                key: 'assets',
                frame: ['blue1', 'red1', 'green1', 'yellow1', 'silver1', 'purple1'],
                frameQuantity: frameQuantity,
                gridAlign: {
                    width: width,
                    height: height,
                    cellWidth: cellWidth,
                    cellHeight: cellHeight,
                    x: (canvas.width - width * cellWidth) / 2 + (cellWidth / 2),
                    y: cellHeight * 2,
                }
            };
        }

        this.bricks = this.physics.add.staticGroup(makeGroupConfig(12, 12, 6, 64, 32));

        this.paddle = this.physics.add
            .image(CENTER, BOTTOM, 'assets', 'paddle1')
            .setImmovable();

        this.ball = this.physics.add
            .image(CENTER, BOTTOM - this.paddle.height, 'assets', 'ball1')
            .setCollideWorldBounds(true)
            .setBounce(1);
        this.ball.setData('onPaddle', true);

        //  Our colliders
        this.physics.add.collider(
            this.ball,
            this.bricks,
            this.hitBrick,
            null,
            this
        );
        this.physics.add.collider(
            this.ball,
            this.paddle,
            this.hitPaddle,
            null,
            this
        );

        this.leftKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.rightKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

    },

    /**
     * Ball hit brick
     * @param {} _ 
     * @param {} brick 
     * @this Phaser.Scene
     */
    hitBrick: function (_, brick) {
        brick.disableBody(true, true);
        this.score += 10;
        this.scoreText.setText(`Score: ${this.score}`);

        if (this.bricks.countActive() === 0) {
            this.resetLevel();
        }
    },

    /**
     * Reset the ball
     * @this Phaser.Scene
     */
    resetBall: function () {
        this.ball.setVelocity(0);
        this.ball.setPosition(this.paddle.x, BOTTOM - this.paddle.height);
        this.ball.setData('onPaddle', true);
    },

    /**
     * Reset level
     * @this Phaser.Scene
     */
    resetLevel: function () {
        this.lives = 3;
        this.score = 0;
        this.resetBall();
        this.livesText.setText('❤'.repeat(this.lives));
        this.scoreText.setText(`Score: ${this.score}`);

        this.bricks.children.each(function (brick) {
            brick.enableBody(false, 0, 0, true, true);
        });
    },

    /**
     * Ball hits the paddle the ball
     * @param {} ball 
     * @param {} paddle 
     * @this Phaser.Scene
     */
    hitPaddle: function (ball, paddle) {
        var diff = 0;

        if (ball.x < paddle.x) {
            //  Ball is on the left-hand side of the paddle
            diff = paddle.x - ball.x;
            ball.setVelocityX(-10 * diff);
        } else if (ball.x > paddle.x) {
            //  Ball is on the right-hand side of the paddle
            diff = ball.x - paddle.x;
            ball.setVelocityX(10 * diff);
        } else {
            //  Ball is perfectly in the middle
            //  Add a little random X to stop it bouncing straight up!
            ball.setVelocityX(2 + Math.random() * 8);
        }
    },

    lostLife: function () {
        if (this.lives <= 0) {
            this.resetLevel();
        } else {
            this.lives--;
            this.livesText.setText('❤'.repeat(this.lives));
            this.resetBall();
        }
    },

    /**
     * Update 
     * @this Phaser.Scene
     */
    update: function () {
        if (this.ball.y > canvas.height) {
            this.lostLife();
        }

        function movePaddle(direction) {
            this.paddle.halfWidth = this.paddle.width / 2;
            //  Keep the paddle within the game
            this.paddle.x = Phaser.Math.Clamp(
                typeof direction == 'number' ? this.paddle.x + direction : direction.x,
                this.paddle.halfWidth,
                canvas.width - this.paddle.halfWidth
            );

            if (this.ball.getData('onPaddle')) {
                this.ball.x = this.paddle.x;
            }
        }

        if (this.leftKey.isDown) {
            movePaddle.call(this, -10);
        }

        if (this.rightKey.isDown) {
            movePaddle.call(this, 10);
        }

        //  Input events
        this.input.on(
            'pointermove',
            function (pointer) {
                movePaddle.call(this, pointer);
            },
            this
        );

        function launchBall() {
            if (this.ball.getData('onPaddle')) {
                this.ball.setVelocity(-75, -300);
                this.ball.setData('onPaddle', false);
            }
        }

        if (this.spaceKey.isDown) {
            launchBall.call(this);
        }

        this.input.on(
            'pointerup',
            launchBall,
            this
        );
    },
});


export default Breakout;