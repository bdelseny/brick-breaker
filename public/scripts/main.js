/** @type {import('../../typing/phaser')} */
/** @type {Phaser.Types.Core.GameConfig} */
import Breakout from './scene/breakout.js';

var config = {
    type: Phaser.WEBGL,
    width: canvas.width,
    height: canvas.height,
    parent: 'phaser-example',
    scene: [Breakout],
    physics: {
        default: 'arcade',
        arcade: {
            gravity: false
        }
    },
};

var game = new Phaser.Game(config);